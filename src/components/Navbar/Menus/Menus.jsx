import React from 'react';
import classes from './Menus.module.css';
import {NavLink} from "react-router-dom";

const Menus = (props) => {
    return(
        <div className={ classes.item }>
            <NavLink to={ props.links } activeClassName={ classes.activeLink }>{ props.name }</NavLink>
        </div>
    );
};

export default Menus;
