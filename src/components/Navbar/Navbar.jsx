import React from 'react';
import classes from './Navbar.module.css';
import Menus from './Menus/Menus';

const Navbar = () => {
  return(
      <nav className={ classes.nav }>
          <Menus links="/profile"  name="Profile" />
          <Menus links="/dialogs" name="Dialogs" />
          <Menus links="/news" name="News" />
          <Menus links="/music" name="Music" />
          <Menus links="/settings" name="Settings" />
      </nav>
  );
};

export default Navbar;
