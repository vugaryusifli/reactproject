import React from 'react'
import classes from './ProfileInfo.module.css';

const ProfileInfo = () => {
    return(
        <div>
            <div className={ classes.cover }>
                <img src="https://www.salto-youth.net/tools/otlas-partner-finding/download/4738/2.jpg" alt="" />
            </div>
            <div className={ classes.descriptionBlock }>
                avatar+description
            </div>
        </div>
    );
};

export default ProfileInfo;
